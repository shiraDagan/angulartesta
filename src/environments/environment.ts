// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyArCfezfTMqM8DKufJ0ecxhdv2lNlIUA54",
    authDomain: "angulartest-fb3ec.firebaseapp.com",
    databaseURL: "https://angulartest-fb3ec.firebaseio.com",
    projectId: "angulartest-fb3ec",
    storageBucket: "angulartest-fb3ec.appspot.com",
    messagingSenderId: "626360355600",
    appId: "1:626360355600:web:8e91f02a8f116eb43d7395",
    measurementId: "G-W5PVNETBYG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
