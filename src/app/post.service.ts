import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable,throwError } from 'rxjs';
import { Post } from './interfaces/post';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';




@Injectable({
  providedIn: 'root'
})
export class PostService {

 
   apiurl = "https://jsonplaceholder.typicode.com/posts"//הכנסת הAPI למשתנה
   
 //  private urlcomments ="https://jsonplaceholder.typicode.com/comments";
  
 private itemsCollection: AngularFirestoreCollection<Post>;

 userCollection:AngularFirestoreCollection = this.db.collection('users');
postCollection:AngularFirestoreCollection;
 
//EX4
  addpost(userId:string,title:string, body:string){
    const post = {title:title, body:body};
    this.userCollection.doc(userId).collection('posts').add(post);
   }



   constructor(private http: HttpClient, private db: AngularFirestore) { 
    this.itemsCollection = db.collection<Post>('classify');//ex6
   }
//EX2
   getPost(){
    return this.http.get<Post[]>(this.apiurl)//הפלט של מה שנקבל מפונקציית גט יהיה מסוג פוסט
   }//this.apiurl הנתונים שפונקציית גט מקבלת

  //  private handleError(res: HttpErrorResponse) {//תמיד כשעושים קריאות משתמשים בפונקציה זו שמנהלת את השגיאות- אומרת מה לעשות איתם
  //   console.error(res.error);
  //   return throwError(res.error || 'Server error');
  // }


  // getposts():Observable<Posts[]> {
  //      return this.http.get(this.URL).pipe(map((response:Posts[]) => response));
           
  //    };

     
   //ex4
   addToFirestore(_title:string, _body:string, _id:number, _userId:string){
    const post = { title:_title, body:_body,id: _id, userId:_userId};
    this.db.collection('posts').add(post);
  }

  //ex5
  //הגדרת הפונקציה שתשלוף לדף זה את פוסטים ג

  getposts():Observable<any[]>{
    var response = this.db.collection('posts').valueChanges({idField:'id'});
    return response;
    
  }
  




}

