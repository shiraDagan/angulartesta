import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public authService:AuthService) { }
  
  userId:string;
  email:string

  ngOnInit() {
    this.authService.getUser().subscribe(
           user => {
            this.userId = user.uid;  
             this.email= user.email;
            console.log(this.userId) ; 
            console.log(this.email) ;
         })
  }

}
