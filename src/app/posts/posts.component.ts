import { Component, OnInit } from '@angular/core';
import { Post } from '../interfaces/post';
import { PostService } from '../post.service';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
 
  id:number;
  title:string;
  body:string;
  // text:string;
  // postid:number;
   userId:string;  

  Posts$: Post[];//כשנקבל את הנתונים מהסרוויס נקלוט אותם לתוך משתנה

  likes:number = 0;

  addLikes(){
    this.likes ++ //likes that belongs to this section...
  }

  constructor(private service:PostService) { }

  // add(title:string,body:string,id:number){
  //    this.service.addpost(this.userId,title,body)
  //    this.postid = id;
  //    this.text = "The post upload successfully"
  //    }



  ngOnInit() {
    this.service.getPost()
    .subscribe(data =>this.Posts$ = data );
//קריאה לפונקציה מה service
  }

  //EX4
  addPostsToFirestore(title,body,id){
    this.service.addToFirestore(title,body,id,this.userId); 
         }

 }
