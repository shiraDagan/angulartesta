import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PostService } from '../post.service';

@Component({
  selector: 'app-save-posts',
  templateUrl: './save-posts.component.html',
  styleUrls: ['./save-posts.component.css']
})
export class SavePostsComponent implements OnInit {
  userId:string;
  posts$;
  constructor( private service:PostService,private authService:AuthService) { }

  ngOnInit() {
    //ex5
   this.authService.user.subscribe(
    user => {
      this.posts$ = this.service.getposts();
     }
   )
  }

}
