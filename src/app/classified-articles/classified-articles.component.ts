import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { ImageService } from '../image.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classified-articles',
  templateUrl: './classified-articles.component.html',
  styleUrls: ['./classified-articles.component.css']
})
export class ClassifiedArticlesComponent implements OnInit {

  constructor(public classifyService:ClassifyService,
    public imageService:ImageService, private router: Router,private route:ActivatedRoute) { }
//classify
  category:string = "Loading...";
  categoryImage:string; 
  categories:object[] = 
  [{id:0,cat: 'business'}, {id:1,cat: 'entertainment'}, {id:2,cat: 'politics'},{id:3,cat: 'sport'}, {id:4,cat: 'tech'}];

  //classify
  onSelect(value){
    this.categoryImage = this.imageService.images[parseInt(value)];
}
  ngOnInit() {
    //classify
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classifyService.doc)
      }
    )
  }

}
